// node.js core modules imports
import * as path from 'path';

// external dependencies imports
import express from 'express';
import morgan from 'morgan';
import * as bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';

// app internal imports
import { htmlEngine } from './utils';
import index from './routes';

// Create & configure express instance
const app = express();

// Set 'html' as view engine
app.engine('html', htmlEngine);
app.set('views', path.resolve(__dirname, '../public/views'));
app.set('view engine', 'html');

// Configure middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.resolve(__dirname, '../public/')));

// Attach 'index' router and disable unnecessary express header
app.use('/', index);
app.set('x-powered-by', false);

export default app;
