import { Router, Request, Response, NextFunction } from 'express';

const index: Router = Router();

/* GET main page */
index.get('/', (request: Request, response: Response, next: NextFunction) => {
  response.render('index', {
    title: 'Stahlos!',
    stahlos: 'assets/stahlos.gif',
  });
});

export default index;
