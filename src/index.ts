// node.js core module imports
import * as http from 'http';

// external deps import statements
import chalk from 'chalk';
import morgan from 'morgan';

// app internal imports
import app from './app';
import { normalizePort, errorEventListener } from './utils';

// Get port from environment
const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

// Create http server & start listening for connections
const logger = morgan('dev');
const server = http.createServer(app);
server.listen(port);

server.on('error', errorEventListener(port));
server.on('listening', onListening);

function onListening() {
  console.log(chalk.green('Stahlos is running on Port ' + port + '!'));
}
