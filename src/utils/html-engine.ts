import * as fs from 'fs';

export default function htmlEngine(
  filePath: string,
  options: any,
  callback: any,
) {
  fs.readFile(filePath, (error, content) => {
    const rendered = content.toString('utf-8');
    return callback(null, rendered);
  });
}
