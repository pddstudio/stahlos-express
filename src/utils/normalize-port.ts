/**
 * Normalize a port into a number, string, or false.
 * Original taken from: https://github.com/Microsoft/vscode-samples/blob/master/node-express-typescript/src/www.ts#L20
 */
export default function normalizePort(val: any): number | string | boolean {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
