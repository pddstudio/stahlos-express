export class ErrorEventListener {
  constructor(private readonly port: any) {}

  /**
   * Event listener for HTTP server "error" event.
   * Original taken from: https://github.com/Microsoft/vscode-samples/blob/master/node-express-typescript/src/www.ts#L39
   */
  public onError(error: any) {
    if (error.syscall !== 'listen') {
      throw error;
    }

    const bind =
      typeof this.port === 'string' ? 'Pipe ' + this.port : 'Port ' + this.port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }
}

const errorEventListener = (port: any) => new ErrorEventListener(port).onError;
export default errorEventListener;
