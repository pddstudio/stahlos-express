# stahlos-express

> Stahlos.js?! A random sample project of using Node, TypeScript & ExpressJS.

[Live Demo of this Project](https://stahlos.pddstudio.io/)

## About the project name

The project name was _kinda_ randomly brainstormed by myself.

It's a _"comprimized variant"_ of the german question "Was ist da los?", mixed with some _south-side badisch/bayrisch dialect_ & mainly inspired by a famous german YouTuber [E IL o T IR i X ™](https://www.youtube.com/user/ELoTRiXHDx) who first came up with this in one of [his videos](https://www.youtube.com/watch?v=MkijwmS_sp0).

## Getting started with development

To get started with development for this project simply follow these few steps:

```sh
# clone this repository to your local machine
git clone https://gitlab.com/pddstudio/stahlos-express.git
# cd into the project directory
cd stahlos-express
# install dependencies with yarn, build the project & start hacking
yarn && yarn start
```

## Contributing

```
TODO: Add contribution information
```

## License

  (c) 2018 - MIT License - Patrick Jung <patrick.pddstudio@gmail.com>
