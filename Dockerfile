FROM node:10.5.0-alpine as builder

WORKDIR /home/node/app
COPY . .
RUN yarn install && yarn build

FROM node:10.5.0-alpine

ENV NODE_ENV=production
WORKDIR /home/node/app

COPY ./package* ./
RUN npm install --production

COPY --from=builder /home/node/app/dist ./dist
COPY --from=builder /home/node/app/public ./public

EXPOSE 3000
CMD [ "node", "./dist" ]
